import { HomeWrapper } from "@/modules/Home/Home.page";

export default function Page() {
  return (
    <main>
      <article>
         <HomeWrapper />
      </article>
    </main>
  );
}
