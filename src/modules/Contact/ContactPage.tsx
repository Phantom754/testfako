/* eslint-disable @next/next/no-img-element */
import { ButtonRight } from '@/common/components/Button'
import React from 'react'

export const ContactPage = () => {
  return (
    <section className="newsletter" id="newsletter">
    <div className="container">
      <figure className="newsletter-banner">
        <img src="./media/svg/newsletter.svg" alt="Illustration art" loading="lazy" className="w-100" />
      </figure>

      <div className="newsletter-content">

        <p className="section-subtitle">Subscribe Our Newsletter</p>

        <h2 className="h2 section-title">The latest resources, sent to your inbox weekly</h2>

        <form action="" className="newsletter-form">
          <input type="email" name="email" required placeholder="Enter your email address" className="input-field" />
          <ButtonRight type='primary' text='Subscribe Now' />
        </form>

      </div>

    </div>
  </section>

  )
}
