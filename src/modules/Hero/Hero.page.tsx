/* eslint-disable react/jsx-no-undef */
/* eslint-disable @next/next/no-img-element */
import { ButtonRight } from "@/common/components/Button";
import React from "react";

export const Hero = () => {
  return (
    <section className="hero" id="home">
      <img
        src="./media/images/shape1.png"
        alt="Illustration art"
        className="shape shape-1"
      />
      <img
        src="./media/images/shape2.png"
        alt="Illustration art"
        className="shape shape-2"
      />
      <img
        src="./media/images/shape3.png"
        alt="Illustration art"
        className="shape shape-3"
      />

      <div className="container">
        <figure className="hero-banner">
          <img
            src="/media/images/hero-banner.png"
            alt="Illustration art"
            loading="lazy"
            className="w-100"
          />
        </figure>
        <div className="hero-content">
          <h2 className="h1 hero-title">Smart-IT solutions MINOME BATONE</h2>
          <p className="section-text">
            Remettez-vous à nous pour votre projet digital, ça on sait faire.
          </p>
          <ButtonRight text="Get Started Now" type="primary" />
        </div>
      </div>
    </section>
  );
};
