/* eslint-disable @next/next/no-img-element */
import { ChevronRight } from "lucide-react"

type Props = {
    title: string,
    path: string
    content: string,
    link: string
}
export const CardFeature = (props:Props) => {
    return (
        <li >
                <div className="features-card">
                  <div className="card-icon">
                    <img
                      src={props.path}
                      alt="Illustration icon"
                    />
                  </div>

                  <h3 className="h3 card-title">{props.title}</h3>

                  <p className="card-text">
                    {props.content}
                  </p>

                  {/* <a href="#" className="card-link">
                    <span>{props.link}</span>
                    <ChevronRight />
                  </a> */}
                </div>
              </li>
    )
}