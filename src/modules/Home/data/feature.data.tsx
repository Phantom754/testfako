export const featureData = [
    {
        path: '/media/images/features-icon-1.png',
        title: 'Développement web',
        contentResume : 'Équipe chevronnée de jeunes experts avec plus de 5 ans d\'expérience.',
        content: '  Notre domaine de prédilection, une équipe complète de jeunes déjà chevronnés de plus de 5 ans dans leur langage et framework respectifs, qui peuvent même vous guider à trouver ce que vous voulez si vous êtes encore hésitant.        ',
        link:'Learn More'
    },
    {
        path: '/media/images/features-icon-2.png',
        title: 'Développement mobile',
        contentResume : 'Accompagnement création applications mobiles iOS/Android adaptées à vos besoins, budget.',
        content: ' Que vous soyez plutôt iOS ou Android ou même les deux, nous sommes à votre disposition pour vous accompagner dans le processus de création d\'applications mobiles adaptées à vos besoins, selon votre budget',
        link:'Learn More'
    },
    {
        path: '/media/images/features-icon-3.png',
        title: 'Site e-commerce',
        contentResume : 'Département Odoo prêt à relever tous vos défis de gestion d\'entreprise.',
        content: ' Notre département Odoo est prêt à relever les défis que vous leur proposez pour tout ce qui est programme de gestion d’entreprise, incluant gestion des employés, comptabilité, CRM, achats, ...',
        link:'Learn More'
    },
    {
        path: '/media/images/features-icon-4.png',
        title: 'Services Architecture et Devops',
        contentResume : 'Experts systèmes, réseaux, DevOps disponibles pour architecture applicative, on-premise/cloud.',
        content: '  Plusieurs ingénieurs systèmes et réseaux et Devops composent notre équipe et peuvent vous fournir leur expertise pour tout service relatif à l\'architecture sous-jacente à vos applications, qu\'ils soient on-premise, virtualisés ou dans le cloud.',
        link:'Learn More'
    }
]