/* eslint-disable @next/next/no-img-element */
import React from 'react'
import { Hero } from './components/Hero'
import { Feature } from './components/Feature'
import { Service } from './components/Searvice'
import { Blog } from './components/Blog'
import { ContactPage } from '../Contact/ContactPage'

export const HomeWrapper = () => {
  return (
     <>
      <Hero /> 
      <Feature/>
      <Service />
      {/* Pour l'instant, garder cela caché jusqu'à la prochaine réunion de la CDC de développement. */}
      {/* <Blog /> */} 
      {/* <ContactPage /> */}
     </> 
  )
}
