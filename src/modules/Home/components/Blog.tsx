import { BadgePercent } from "lucide-react"
export const Blog = () => {
  return (
    <section className="blog" id="blog">
    <div className="container">

      <p className="section-subtitle">Latest News</p>

      <h2 className="h2 section-title">Our latest articles & resources</h2>

      <ul className="blog-list">

        <li>
          <div className="blog-card">

            <figure className="blog-banner">
              <a href="#">
                <img src="./media/images/blog-1.jpg" alt="How is technology working with new things?"
                  loading="lazy" className="w-100" />
              </a>
            </figure>

            <div className="blog-content">

              <ul className="blog-meta-list">

                <li className="blog-meta-item">
                <BadgePercent />

                  <a href="#" className="blog-meta-link">Technology</a>
                </li>

                <li className="blog-meta-item">
                  <BadgePercent /> 
                  <time className="blog-meta-time" dateTime="2022-02-25">25 Feb, 2022</time>
                </li>

              </ul>

              <h3 className="h3 blog-title">
                <a href="#">How is technology working with new things?</a>
              </h3>

            </div>

          </div>
        </li>

        <li>
          <div className="blog-card">

            <figure className="blog-banner">
              <a href="#">
                <img src="./media/images/blog-2.jpg" alt="Top 10 important tips on IT services & design"
                  loading="lazy" className="w-100" />
              </a>
            </figure>

            <div className="blog-content">

              <ul className="blog-meta-list">

                <li className="blog-meta-item">
                <BadgePercent />

                  <a href="#" className="blog-meta-link">Design</a>
                </li>

                <li className="blog-meta-item">
                <BadgePercent />

                  <time className="blog-meta-time" dateTime="2022-02-25">25 Feb, 2022</time>
                </li>

              </ul>

              <h3 className="h3 blog-title">
                <a href="#">Top 10 important tips on IT services & design</a>
              </h3>

            </div>

          </div>
        </li>

        <li>
          <div className="blog-card">

            <figure className="blog-banner">
              <a href="#">
                <img src="./media/images/blog-3.jpg" alt="How our company works in different ways" loading="lazy"
                  className="w-100" />
              </a>
            </figure>

            <div className="blog-content">

              <ul className="blog-meta-list">

                <li className="blog-meta-item">
                  <BadgePercent />
                  <a href="#" className="blog-meta-link">Startup</a>
                </li>

                <li className="blog-meta-item">
                <BadgePercent />

                  <time className="blog-meta-time" dateTime="2022-02-25">25 Feb, 2022</time>
                </li>

              </ul>

              <h3 className="h3 blog-title">
                <a href="#">How our company works in different ways</a>
              </h3>

            </div>

          </div>
        </li>

      </ul>

    </div>
  </section>
  )
}
