import { featureData } from "../data/feature.data";
import { CardFeature } from "@/modules/features/components/CardFeature";

export const Feature = () => {
  return (
    <section className="features" id="features">
      <div className="container">
        <p className="section-subtitle">Dites-nous ce que vous cherchez, un petit guide?</p>
        <h2 className="h2 section-title">
            Dites-nous …
        </h2>
        <ul className="features-list">
          {featureData.map((data, index) => {
            return (
             <CardFeature key={index} {...data} />
            );
          })}
        </ul>
      </div>
    </section>
  );
};
