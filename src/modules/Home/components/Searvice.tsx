/* eslint-disable @next/next/no-img-element */
import { Check } from "lucide-react"

export const Service = () => {
  return (
    <section className="service" id="service">
        <div className="container" >
          <figure className="service-banner">
            <img src="./media/images/service-1.png" alt="Illustration art" loading="lazy" className="w-100" />
          </figure>
          <div className="service-content" id="programmationLangage">
            {/* <p className="section-subtitle">Services We Offer</p> */}
            <h2 className="h2 section-title"> Langage de programmation</h2>
            <ul className="service-list">
              <li className="service-item">
                <div className="service-item-icon">
                  <Check />
                </div>
                <h3 className="h3 service-item-title">Javascript</h3>
              </li>
              <li className="service-item">
                <div className="service-item-icon">
                  {/* <ion-icon name="checkmark-outline"></ion-icon> */}
                  <Check />
                </div>
                <h3 className="h3 service-item-title">PHP</h3>
              </li>
              <li className="service-item">
                <div className="service-item-icon">
                  {/* <ion-icon name="checkmark-outline"></ion-icon> */}
                  <Check />
                </div>
                <h3 className="h3 service-item-title">Python</h3>
              </li>
              <li className="service-item">
                <div className="service-item-icon">
                  {/* <ion-icon name="checkmark-outline"></ion-icon> */}
                  <Check />
                </div>
                <h3 className="h3 service-item-title">Dart</h3>
              </li>
            </ul>
          </div>
          <figure className="service-banner">
            <img src="./media/images/service-2.png" alt="Illustration art" loading="lazy" className="w-100" />
          </figure>
          <div className="service-content" id="competenceTechno">
            {/* <p className="section-subtitle">Our Services</p> */}
            <h2 className="h2 section-title">Technologies maîtrisées</h2>
            <ul className="service-list">
              <li className="service-item">
                <div className="service-item-icon">
                <Check />
                </div>
                <h3 className="h3 service-item-title">WordPress, ERP Odoo</h3>
              </li>
              <li className="service-item">
                <div className="service-item-icon">
                <Check />
                </div>
                <h3 className="h3 service-item-title">Symfony, Laravel, CodeIgniter</h3>
              </li>
              <li className="service-item">
                <div className="service-item-icon">
                <Check />
                </div>
                <h3 className="h3 service-item-title">Node.js, Python</h3>
              </li>
              <li className="service-item">
                <div className="service-item-icon">
                <Check />
                </div>
                <h3 className="h3 service-item-title">MySQL, PostgreSQL, React.js, Next.js, Flutter</h3>
              </li>
            </ul>
          </div>
        </div>
      </section>
  )
}
