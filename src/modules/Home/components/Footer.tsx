import React from "react";

export const Footer = () =>{

    const footerStyle: React.CSSProperties = {
        marginTop: "1%",
        marginBottom: "1%"
    };

    return(
        <footer style={footerStyle}>
            <div className="container">
                <div className="footer-content">
                    <div className="footer-section">
                         <p>&copy; {new Date().getFullYear()} Make Lucid. All rights reserved.</p>
                    </div>
                </div>
                
            </div>
        </footer>
    );
}