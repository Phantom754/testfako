import { ChevronRight } from 'lucide-react'
import React, { FC } from 'react'

export const ButtonRight: FC<{type?: 'primary' | 'secondary',text:string}> = ({type='primary',text}) => {
  return (
    <button className={`btn btn-${type}`}>
        <span>{text}</span>
        <ChevronRight />
      </button>
  )
}
