/* eslint-disable @next/next/no-img-element */
import { ButtonRight } from '@/common/components/Button'
import { ChevronRight, Menu, X } from 'lucide-react'
import React from 'react'
import Image from 'next/image';


export const Header = () => {
  const logoMakeLucidOne = "/media/images/logoMakeLucidOne.png";

  return (
    <header className="header" data-header>
    <div className="container">

      <a href="#" className="logo container">
        <Image src={logoMakeLucidOne} alt="Make Lucid Logo" width={120} height={50} />
       
      </a>

      <button className="nav-toggle-btn" data-nav-toggle-btn>
        {/* <ion-icon name="menu-outline" class="open"></ion-icon> */}
        <Menu  className="open" />
        <X  className="close"/>
      </button>

      <nav className="navbar">
        <div className="container">
          <ul className="navbar-list">

            <li>
              <a href="#home" className="navbar-link" data-navbar-link>Accueil</a>
            </li>

            <li>
              <a href="#features" className="navbar-link" data-navbar-link>À propos</a>
            </li>

            <li>
              <a href="#service" className="navbar-link" data-navbar-link>Services</a>
            </li>
            <li>
              <a href="#contact" className="navbar-link" data-navbar-link>Contact</a>
            </li>

            {/* <li>
              <a href="#blog" className="navbar-link" data-navbar-link>Blog</a>
            </li>

            <li>
              <a href="#newsletter" className="navbar-link" data-navbar-link>Newsletter</a>
            </li> */}

          </ul>
        </div>
      </nav>
      {/* <ButtonRight text='Get Started' type='primary'/> */}
    </div>
  </header>
  )
}
