/* eslint-disable @next/next/no-img-element */
import { quickLinks } from "@/common/data";
import {  Instagram, MapPinned,Mail, Facebook  } from "lucide-react";
import React from "react";
import Image from 'next/image';

export const Footer = () => {

  const logoMakeLucidOne = "/media/images/logoMakeLucidOne.png";

  return (
    <footer className="footer">
      <div className="footer-top">
        <div className="container" id="contact">
          <div className="footer-brand">
            <a href="#" className="logo container">
              <Image src={logoMakeLucidOne} alt="Make Lucid Logo" width={100} height={50} />
            </a>

            <p className="section-text">
            N'hésitez surtout pas à prendre contact avec nous pour des informations complémentaires, nous nous ferons un plaisir de vous répondre dans les plus brefs délais.
            </p>

            <div className="location-text">
              <div className="location-icon">
                <MapPinned />
              </div>

              <address className="address">Antananarivo, Madagascar</address>
            </div>
          </div>

          <div className="quicklink-box">
            <p className="h3 quicklink-title">Liens Rapides</p>

            <ul className="quicklink-list">
              {quickLinks.map((quick) => {
                return (
                  <li key={quick?.id}>
                    <a href={`#${quick.link}`} className="quicklink">
                      {quick?.label}
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>

          <div className="contact">
            <p className="h3 contact-title">Contactez-nous</p>

            <ul className="contact-list">
              <li className="contact-item">
                <div className="contact-icon">
                  <Mail />
                </div>

                <a href="mailto:makelucid7@gmail.com" className="contact-link">
                  makelucid7@gmail.com
                </a>
              </li>

              <li className="contact-item">
                <div className="contact-icon">
                  <Facebook />
                </div>

                <a href="https://web.facebook.com/profile.php?id=61556741530531&locale=fo_FO" className="contact-link">
                  Make Lucid
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="footer-bottom">
        <div className="container">
          <p className="copyright">
              &copy; {new Date().getFullYear()} Make Lucid. All rights reserved.
          </p>

        </div>
      </div>
    </footer>
  );
};
